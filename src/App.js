import React from 'react';

import { Landing } from './Pages';

import './App.css';

function App() {
  return (
    <div>
      <Landing />
    </div>
  );
}

export default App;
