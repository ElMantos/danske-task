const validateString = value => !(value && value.length >= 1 && value.length <= 10 && /^[a-z0-9]+$/i.test(value));

export default {
    validateString
}