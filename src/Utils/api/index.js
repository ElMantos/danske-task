import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

const getRandom = () => Math.random() * 10;

const api = axios.create({
    baseURL: 'https://danske-aaff8.firebaseio.com/'
});

const mock = new MockAdapter(api);

const personUrl = new RegExp(`person/*`);
const facilityUrl = new RegExp(`facility/*`);
const exposureUrl = new RegExp(`exposure/*`)

mock.onGet(personUrl).reply(200, {
    val1: getRandom(),
    val2: getRandom(),
});

mock.onGet(facilityUrl).reply(200, {
    val3: getRandom(),
    val4: getRandom(),
});

mock.onGet(exposureUrl).reply(200, {
    val5: getRandom(),
});

export default api;

