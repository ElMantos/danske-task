import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

const Input = ({ input, meta: { error, touched } }) => (
    <div className='field_input text-center'>
        <input className="rounded-full border_blue text-center" {...input} />
        {touched && error && <span>Input value has to be alphanumeric string between 1 and 10 characters</span>}
    </div>
);

Input.propTypes = {
    input: PropTypes.shape({}).isRequired,
    meta: PropTypes.shape({}).isRequired,
}

export default Input;
