import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

const Backdrop = ({ onClick, value }) => (
    <div className="flex justify-center items-center backdrop" onClick={onClick}>
        <div className="flex justify-center items-center modal">{value}</div>
    </div>
);

Backdrop.propTypes = {
    onClick: PropTypes.func.isRequired,
    value: PropTypes.number.isRequired
};

export default Backdrop;