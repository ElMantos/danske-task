import React from 'react'
import PropTypes from 'prop-types';

import './style.css';

const Button = ({ type, title }) => (<button className="rounded-full field_button border_blue" type={type}>{title}</button>);

Button.propTypes = {
    type: PropTypes.string,
    title: PropTypes.string.isRequired
}

Button.defaultProptypes = {
    type: 'submit'
}

export default Button;