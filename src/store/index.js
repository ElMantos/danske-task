import * as actionTypes from './actionTypes';
import reducer from './reducer';

export {
    actionTypes,
    reducer
};