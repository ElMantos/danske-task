import * as types from './actionTypes';

const defaultState = {
    reponseValue: null,
    showBackdrop: false
}

const reducer = (state = defaultState, action) => {
    switch (action.type) {
        case types.HIDE_BACKDROP:
            return {
                ...state,
                showBackdrop: false
            };

        case types.ADD_VALUES:
            return {
                ...state,
                reponseValue: action.value,
                showBackdrop: true
            };

        default:
            return state;
    }
}

export default reducer;