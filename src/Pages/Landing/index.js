import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { Input, Button, Backdrop } from '../../Components';
import { Form, Field } from 'react-final-form';
import { validationRules, api } from '../../Utils';
import { actionTypes } from '../../store';

import './style.css';

const Landing = ({ showBackdrop, getApiData, responseValue, hideBackDrop, displayBackdrop }) => {
    if (showBackdrop && getApiData) {
        return <Backdrop onClick={hideBackDrop} value={responseValue} />;
    }
    return (
        <div className="page_landing">
            <Form
                onSubmit={({ userInput }) => {
                    getApiData(userInput);
                }}
                render={({ handleSubmit }) => (
                    <form className="justify-center h-100 flex flex-column items-center" onSubmit={handleSubmit}>
                        <Field validate={(value) => validationRules.validateString(value)} name="userInput" component={Input} />
                        <Button title="SUBMIT" />
                    </form>
                )}
            />
        </div>
    );
}

Landing.propTypes = {
    showBackdrop: PropTypes.bool.isRequired,
    responseValue: PropTypes.number,
    hideBackDrop: PropTypes.func.isRequired,
    getApiData: PropTypes.func.isRequired,
}

const enhance = connect(
    state => ({
        showBackdrop: state.showBackdrop,
        responseValue: state.reponseValue
    }),
    dispatch => ({
        hideBackDrop: () => dispatch({ type: actionTypes.HIDE_BACKDROP }),
        getApiData: async (value) => {
            await api.get(`/person/${value}`).then((data) => {
                const value_one = data.data.val1;
                const value_two = data.data.val2;
                api.get(`/facility/${value_one}`).then(data => {
                    const value_three = data.data.val3;
                    api.get(`/exposure/${value_two}`).then(data => dispatch({ type: actionTypes.ADD_VALUES, value: value_three * data.data.val5 }));
                })
            });
        },
    })
);

export default enhance(Landing);